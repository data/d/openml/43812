# OpenML dataset: Performance-Prediction

https://www.openml.org/d/43812

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
The Dataset contains the summary of each player.
on the basis of the summary of each player you have to predict the target variable
Target Variable:
1-Signifies whether a player has a career of 5 years or more.
0-Signifies the career of the player is shorter than 5 years.
Some features  other than target variable:
GamesPlayed
PointsPerGame
3PointMade  etc.
Inspiration
-This dataset is helpful in finding the features which are helpful in predicting that how long a player can play.
-Use EDA to get the insight information about the features.
-Create a classification model to predict the target variable.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43812) of an [OpenML dataset](https://www.openml.org/d/43812). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43812/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43812/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43812/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

